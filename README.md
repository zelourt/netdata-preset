## Install by script
```bash
$ curl -s -L https://gitlab.com/zelourt/netdata-preset/-/raw/main/script.sh | bash
```

## Setup netdata

```bash
$ docker compose up --build -d
```

#### Setup traefik labels:
```
- traefik.enable
- traefik.http.routers.netdata.rule
- traefik.http.middlewares.netdata-auth.basicauth.users=
```

## Gen passwords (traefik)
```bash
$ sudo apt install apache2-utils
$ echo $(htpasswd -nbB <USER> "<PASS>") | sed -e s/\\$/\\$\\$/g
```

